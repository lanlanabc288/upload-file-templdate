<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ImageController extends Controller
{
    public function setTemplateImageUrl(Request $request)
    {
        $image = $request->image;
        $name = Str::uuid() . '.png';
        $image->move(public_path('images'), $name);
        $src = '<iframe src="/images/' . $name . '"class="picture" frameborder="0" title="Iframe Example" id="pic" />';
        return response()->json($src);
    }
}
