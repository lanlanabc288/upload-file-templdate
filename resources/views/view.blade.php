<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>View Image</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <style>
        .container {
            margin-top: 100px
        }

        iframe {
            margin-left: 100px;
            padding-top: 100px;
            height: 700px;
            width: 960px;
        }
    </style>
</head>

<body>
    <div class="container">
        <div>
            <form method="POST" id="form">
                <input type="file" id="image" name="image">
                <input type="submit" value="Upload" id="submit-image">
                <input type="button" id="reset" value="Reset to Họk Pàii">
            </form>
        </div>
        <iframe src="https://i.pinimg.com/564x/56/30/b4/5630b46c695e994b70500abe3dec11d9.jpg" frameborder="0"
            title="Iframe Example" id="pic" class="picture">
        </iframe>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function(e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#reset').click(function(e) {
            e.preventDefault();
            location.reload();
        });

        $('#form').on('submit', function(e) {
            e.preventDefault();
            $('.picture').remove();

            var formData = new FormData();
            formData.append('image', $('#image'));
            url = 'upload';
            $.ajax({
                type: 'POST',
                url: url,
                data: new FormData(this),
                cache: false,
                contentType: false,
                processData: false,
                success: (data) => {
                    $('.container').append(data);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        })
    });
</script>

</html>
