$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#submit-image').on('click', function () {
        uploadFile();
    })

    function uploadFile() {

        let dataImage = $('#image');

        $.ajax({
            'url': "{{ url('upload')}}",
            'type': 'POST',
            'data': {
                'dataImage': dataImage
            },
            'success': function (data) {

            },
            'error': function (request, error) {
                alert("Có thể là ảnh không hỗ trợ :DDDD");
            }
        });
    }

});